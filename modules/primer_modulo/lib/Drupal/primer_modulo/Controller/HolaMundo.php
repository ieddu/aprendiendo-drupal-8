<?php
/**
 * @file
 * Contains \Drupal\primer_modulo\Controller\HolaMundo.
 */
namespace Drupal\primer_modulo\Controller;
use Drupal\Core\Controller\ControllerBase;
/**
 * Controller routines for hello module routes.
 */
class HolaMundo extends ControllerBase {
    /**
     * Return the 'Text the example' page.
     *
     * @return string
     *   A render array containing our 'Hola Mundo' page content.
     */
    public function holaMundo() {
        $output = array();
        $html = "<h2>" . t("Hola Mundo!") . "</h2>";
        $html .= '<p>Lorem Ipsum es simplemente el texto de relleno '
                . 'de las imprentas y archivos de texto. Lorem Ipsum '
                . 'ha sido el texto de relleno estándar de las industrias '
                . 'documentos electrónicos, quedando esencialmente igual '
                . 'al original. Fue popularizado en los 60s con la creación '
                . 'de las hojas "Letraset", las cuales contenian pasajes de '
                . 'Lorem Ipsum, y más recientemente con software de autoedición, '
                . 'como por ejemplo Aldus PageMaker, el cual incluye versiones '
                . 'de Lorem Ipsum.</p>';
          
        $output['hello'] = array(
            '#markup' => $html,
        );
        return $output;   
    }
}