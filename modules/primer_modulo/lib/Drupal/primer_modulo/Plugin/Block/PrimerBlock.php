<?php
/**
 * @file
 * Contains \Drupal\yourmodule\Plugin\Block\YourModuleBlock.
 */
namespace Drupal\primer_modulo\Plugin\Block;
use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;
/**
 * Provides a simple block.
 *
 * @Block(
 *   id = "primer_modulo_block",
 *   admin_label = @Translation("Mi primer modulo")
 * )
 */
class PrimerBlock extends BlockBase {
  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {  
    return array(
      '#children' => 'Este es mi primer bloque!!!',
    );
  }
  /**
   * Implements \Drupal\block\BlockBase::access().
   */
  public function access(AccountInterface $account) {
    return $account->hasPermission('access content');
  }
}